//
//  TryingMixedLayoutsTests.swift
//  TryingMixedLayoutsTests
//
//  Created by fadi on 6/29/18.
//  Copyright © 2018 fadi. All rights reserved.
//

import XCTest
@testable import TryingMixedLayouts

class TryingMixedLayoutsTests: XCTestCase {
    
    func testGenerateConstraints() {
        let view1 = UIView()
        let view2 = UIView()
        let view3 = UIView()
        let constraints = generateConstraints(view1, constraintDescriptions: [
            (view2, [(.left, .equal, .right, 16.0, 1.5),
                     (.top, .equal, .bottom, 12.0, 1.0)]),
            (view3, [(.top, .equal, .top, 26.0, 2.5),
                     (.right, .equal, .right, 22.0, 2.0)]),
            ])
        
        XCTAssertTrue(constraints.contains(where: {
            return ($0.constant == 16.0) && ($0.firstItem === view1) && ($0.secondItem === view2) &&
                ($0.multiplier == 1.5) && ($0.firstAttribute == .left) && ($0.secondAttribute == .right)
        }))
        XCTAssertTrue(constraints.contains(where: {
            return ($0.constant == 12.0) && ($0.firstItem === view1) && ($0.secondItem === view2) &&
                ($0.multiplier == 1.0) && ($0.firstAttribute == .top) && ($0.secondAttribute == .bottom)
        }))
        XCTAssertTrue(constraints.contains(where: {
            return ($0.constant == 26.0) && ($0.firstItem === view1) && ($0.secondItem === view3) &&
                ($0.multiplier == 2.5) && ($0.firstAttribute == .top) && ($0.secondAttribute == .top)
        }))
        XCTAssertTrue(constraints.contains(where: {
            return ($0.constant == 22.0) && ($0.firstItem === view1) && ($0.secondItem === view3) &&
                ($0.multiplier == 2.0) && ($0.firstAttribute == .right) && ($0.secondAttribute == .right)
        }))
        // Some negative tests
        XCTAssertFalse(constraints.contains(where: {
            return ($0.constant == 22.0) && ($0.firstItem === view1) && ($0.secondItem === view3) &&
                ($0.multiplier == 1.0) && ($0.firstAttribute == .top) && ($0.secondAttribute == .bottom)
        }))
        XCTAssertFalse(constraints.contains(where: {
            return ($0.constant == 12.0) && ($0.firstItem === view1) && ($0.secondItem === view3) &&
                ($0.multiplier == 2.0) && ($0.firstAttribute == .top) && ($0.secondAttribute == .bottom)
        }))
    }
    
}
