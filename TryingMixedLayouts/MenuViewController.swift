//
//  MenuViewController.swift
//  TryingMixedLayouts
//
//  Created by fadi on 7/1/18.
//  Copyright © 2018 fadi. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    // We don't need this except for creation of the third view controller
    // which is created in code
    @IBAction func showTheThirdView(_ sender: Any) {
        show(LayoutedByCodeViewController(), sender: nil)
    }
}
