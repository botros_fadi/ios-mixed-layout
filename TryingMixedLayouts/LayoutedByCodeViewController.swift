//
//  LayoutedByCodeViewController.swift
//  TryingMixedLayouts
//
//  Created by fadi on 7/1/18.
//  Copyright © 2018 fadi. All rights reserved.
//

import UIKit

/// Just a very simple helper function that installs constraints.
///
/// This function also sets the `translatesAutoresizingMaskIntoConstraints` to `false`.
///
/// In the `constraintDescriptions` parameter, one should put a view, then describe the constraint.
///
/// **EXAMPLE:**
///
///     prepareViewConstraints(view1, constraintDescriptions: [
///         (view2, [(.top, .equal, .bottom, 0.0, 1.0)]) // view1.top = (view2.bottom * 1.0) + 0.0
///     ])
///
///
/// - Parameters:
///   - view: The left hand side view to install constraints for
///   - constraintDescriptions: The constraints description
///
func prepareViewConstraints(_ view: UIView, constraintDescriptions: [(UIView, [(NSLayoutAttribute, NSLayoutRelation, NSLayoutAttribute, CGFloat, CGFloat)])]) {
    view.translatesAutoresizingMaskIntoConstraints = false
    generateConstraints(view, constraintDescriptions: constraintDescriptions).forEach({$0.isActive = true})
}

/// Generates constraints from description, it maybe more convenient to use `previewViewConstraints(_:constraintDescriptions:)`
/// This is provided mainly for testing purposes, one must design his code to be testable.
///
/// - Parameters:
///   - view: The left hand side view to generate constraints for
///   - constraintDescriptions: The constraints description
/// - Returns: An array of `NSLayoutConstraint`: generated constraints
func generateConstraints(_ view: UIView, constraintDescriptions: [(UIView, [(NSLayoutAttribute, NSLayoutRelation, NSLayoutAttribute, CGFloat, CGFloat)])]) -> [NSLayoutConstraint] {
    
    return constraintDescriptions.reduce([NSLayoutConstraint]()) { array, viewWithAssociatedConstraints in
        return array + viewWithAssociatedConstraints.1.map({NSLayoutConstraint.init(item: view, attribute: $0.0, relatedBy: $0.1, toItem: viewWithAssociatedConstraints.0, attribute: $0.2, multiplier: $0.4, constant: $0.3)})
    }
}

class LayoutedByCodeViewController: UIViewController {
    
    weak var middleOne: UIView!
    
    weak var childConstrainedView: UIView!
    
    override func loadView() {
        view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.white
        
        let upperView = UIView(frame: CGRect.zero)
        view.addSubview(upperView)
        upperView.backgroundColor = UIColor.blue
        prepareViewConstraints(upperView, constraintDescriptions: [
            (view, [(.top, .equal, .top, 0.0, 1.0),
                    (.left, .equal, .left, 0.0, 1.0),
                    (.right, .equal, .right, 0.0, 1.0),
                    (.height, .equal, .height, 0.0, 0.25)
                ])
            ])
        
        let lowerView = UIView(frame: CGRect.zero)
        view.addSubview(lowerView)
        lowerView.backgroundColor = UIColor.red
        prepareViewConstraints(lowerView, constraintDescriptions: [
            (view, [(.bottom, .equal, .bottom, 0.0, 1.0),
                    (.left, .equal, .left, 0.0, 1.0),
                    (.right, .equal, .right, 0.0, 1.0),
                    (.height, .equal, .height, 0.0, 0.25)
                ])
            ])
        let middleOne = UIView(frame: CGRect.zero)
        view.addSubview(middleOne)
        middleOne.backgroundColor = UIColor.yellow
        prepareViewConstraints(middleOne, constraintDescriptions: [
            (view, [(.left, .equal, .left, 0.0, 1.0),
                    (.right, .equal, .right, 0.0, 1.0)]),
            (upperView, [(.top, .equal, .bottom, 0.0, 1.0)]),
            (lowerView, [(.bottom, .equal, .top, 0.0, 1.0)])
            ])
        self.middleOne = middleOne
        
        let constrainedView = UIView(frame: CGRect.zero)
        constrainedView.backgroundColor = UIColor.cyan
        let insideLabel = UILabel(frame: CGRect.zero)
        insideLabel.text = "Text"
        constrainedView.addSubview(insideLabel)
        prepareViewConstraints(insideLabel, constraintDescriptions: [
            (constrainedView, [(.centerY, .equal, .centerY, 0.0, 1.0),
                               (.left, .equal, .left, 16.0, 1.0)])
            ])
        self.middleOne.addSubview(constrainedView)
        // constrainedView is provided in code
        // So it DEFAULTED to translatesAutoresizingMaskIntoConstraints = true
        // So this line is NOT necessary.
        //    constrainedView.translatesAutoresizingMaskIntoConstraints = true
        childConstrainedView = constrainedView
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        childConstrainedView.frame = middleOne.bounds.insetBy(dx: 100, dy: 100)
        childConstrainedView.frame = childConstrainedView.frame.offsetBy(dx: 10, dy: 0)
    }
}
