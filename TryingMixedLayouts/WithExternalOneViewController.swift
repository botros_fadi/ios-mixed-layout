//
//  WithExternalOneViewController.swift
//  TryingMixedLayouts
//
//  Created by fadi on 6/29/18.
//  Copyright © 2018 fadi. All rights reserved.
//

import UIKit

class WithExternalOneViewController: UIViewController {
    @IBOutlet var otherView: UIView!
    @IBOutlet weak var container: UIView!
    
    override func loadView() {
        super.loadView()
        container.addSubview(otherView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        otherView.frame = container.bounds.insetBy(dx: 50, dy: 20)
        otherView.frame = otherView.frame.offsetBy(dx: 0, dy: 30)
    }
}

