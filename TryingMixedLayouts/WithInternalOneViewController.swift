//
//  WithInternalOneViewController.swift
//  TryingMixedLayouts
//
//  Created by fadi on 6/29/18.
//  Copyright © 2018 fadi. All rights reserved.
//

import UIKit

class WithInternalOneViewController: UIViewController {
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var inside: UIView!
    
    override func loadView() {
        super.loadView()
        // Debugging views shows that the WithExternalOneViewController gives
        // good layouts because there are translation of rects to constraints
        inside.translatesAutoresizingMaskIntoConstraints = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        inside.frame = container.bounds.insetBy(dx: 50, dy: 100)
        inside.frame = inside.frame.offsetBy(dx: 0, dy: 30)
    }
}

