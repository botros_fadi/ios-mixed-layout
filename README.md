#  ios-mixed-layout

This is a demonstration for how one could make a mixed layout in his views

## Types of layout in iOS:

  - Autolayout
    - Constraint-based
    - Stack-view based
    - Autoresizing based
  - Frame based layout (was the oldest way, but the most control to the views)
 
## What this example demonstrate:

  - Using storyboards to constrain your layouts
  - Using code to make your layout,   `LayoutedByCodeViewController.swift`
  - How to instantiate a UIViewController which is defined from code
  - Unit testing, and how you could design the code to be testable
    - `prepareViewConstraints` was a function that makes the constraints and activate it, so it is NOT testable,  so the solution was to divide it into 2 functions, one of them just generates the constraints so it could be tested (Well, this is NOT the best solution also, because good test code must know **NOTHING** about the framework, which here is `UIKit`, so a protocol-oriented approach could be better.)
 

